﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BancariaSistemaKinal.Models
{
    public class TipoTarjeta
    {
        public int ID { get; set; }
        [Display (Name="Tipo Tarjeta")]
        public string TipoDeTarjeta { get; set; }

    }
}