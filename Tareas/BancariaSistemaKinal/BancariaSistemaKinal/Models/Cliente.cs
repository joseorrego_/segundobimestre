﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BancariaSistemaKinal.Models
{
    public class Cliente
    {
        public int ID { get; set; }

        [Display(Name = "Nombre")]
        public string Nombre { get; set; }

        [Display(Name = "Apellido")]
        public string Apellido { get; set; }

        [Display(Name = "Direccion")]
        public string Direccion { get; set; }

        [Display(Name = "Correo Electronico")]
        public string CorreoElectronico { get; set; }

        [Display(Name = "Telefono Casa")]
        public int TelefonoCasa { get; set; }

        [Display(Name = "Telefono Celular")]
        public int Celular { get; set; }

        [Display(Name = "Referencia 1")]
        public string Referencia1 { get; set; }

        [Display(Name = "Numero Referencia 1")]
        public int NumeroReferencia1 { get; set; }

        [Display(Name = "Referencia 2")]
        public string Referencia2 { get; set; }

        [Display(Name = "Numero de la Referencia 2")]
        public string NumeroReferencia2 { get; set; }

    }
}