﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BancariaSistemaKinal.Startup))]
namespace BancariaSistemaKinal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
