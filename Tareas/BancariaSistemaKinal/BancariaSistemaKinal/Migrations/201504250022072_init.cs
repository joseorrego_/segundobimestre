namespace BancariaSistemaKinal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Transaccions", "Fecha", c => c.String());
            DropColumn("dbo.Transaccions", "Lugar");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Transaccions", "Lugar", c => c.String());
            AlterColumn("dbo.Transaccions", "Fecha", c => c.DateTime(nullable: false));
        }
    }
}
