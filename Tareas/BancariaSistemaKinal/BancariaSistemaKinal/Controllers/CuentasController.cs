﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BancariaSistemaKinal.Models;

namespace BancariaSistemaKinal.Controllers
{
    public class CuentasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        [Authorize (Roles="Admin, User")]
        // GET: /Cuentas/
        public ActionResult Index()
        {
            var cuentas = db.Cuentas.Include(c => c.Cliente).Include(c => c.TipoCuenta);
            return View(cuentas.ToList());
        }

        // GET: /Cuentas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cuenta cuenta = db.Cuentas.Find(id);
            if (cuenta == null)
            {
                return HttpNotFound();
            }
            return View(cuenta);
        }

        [Authorize (Roles="Admin")]
        // GET: /Cuentas/Create
        public ActionResult Create()
        {
            ViewBag.ClienteID = new SelectList(db.Clientes, "ID", "Nombre");
            ViewBag.TipoCuentaID = new SelectList(db.TipoCuentas, "ID", "Tipo");
            return View();
        }

        // POST: /Cuentas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,Nombre,Estado,SaldoInicial,Numerodelacuenta,ClienteID,TipoCuentaID")] Cuenta cuenta)
        {
            if (ModelState.IsValid)
            {
                db.Cuentas.Add(cuenta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ClienteID = new SelectList(db.Clientes, "ID", "Nombre", cuenta.ClienteID);
            ViewBag.TipoCuentaID = new SelectList(db.TipoCuentas, "ID", "Tipo", cuenta.TipoCuentaID);
            return View(cuenta);
        }

        [Authorize(Roles = "Admin")]
        // GET: /Cuentas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cuenta cuenta = db.Cuentas.Find(id);
            if (cuenta == null)
            {
                return HttpNotFound();
            }
            ViewBag.ClienteID = new SelectList(db.Clientes, "ID", "Nombre", cuenta.ClienteID);
            ViewBag.TipoCuentaID = new SelectList(db.TipoCuentas, "ID", "Tipo", cuenta.TipoCuentaID);
            return View(cuenta);
        }

        // POST: /Cuentas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,Nombre,Estado,SaldoInicial,Numerodelacuenta,ClienteID,TipoCuentaID")] Cuenta cuenta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cuenta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ClienteID = new SelectList(db.Clientes, "ID", "Nombre", cuenta.ClienteID);
            ViewBag.TipoCuentaID = new SelectList(db.TipoCuentas, "ID", "Tipo", cuenta.TipoCuentaID);
            return View(cuenta);
        }

        [Authorize(Roles = "Admin")]
        // GET: /Cuentas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cuenta cuenta = db.Cuentas.Find(id);
            if (cuenta == null)
            {
                return HttpNotFound();
            }
            return View(cuenta);
        }

        // POST: /Cuentas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Cuenta cuenta = db.Cuentas.Find(id);
            db.Cuentas.Remove(cuenta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
